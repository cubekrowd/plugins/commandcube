package net.cubekrowd.commandcube.placeholder;

import net.cubekrowd.commandcube.CommandCubePlugin;
import net.cubekrowd.commandcube.Placeholder;
import net.md_5.bungee.api.CommandSender;

import java.util.Arrays;

public class PlaceholderIndex extends Placeholder {
    private final CommandCubePlugin plugin;

    public PlaceholderIndex(CommandCubePlugin plugin) {
        super(plugin, "index", "Placeholder to get value off of an array. Starts at 1. Array looks like: 'value1' 'value2'...", Arrays.asList("<number> <String[]>"));
        this.plugin = plugin;
    }

    public String getValue(CommandSender sender, String[] args) {
        if (args.length >= 2) {
            int get;
            try {
                get = Integer.parseInt(args[0]);
            } catch (Exception e) {
                return "NaN";
            }
            if (get < 1 || get > args.length) {
                return "Number too big or too small on index.";
            }

            String[] index = Arrays.copyOfRange(args, 1, args.length);
            String values = String.join(" ", index);
            values = values.substring(1, values.length() - 2);
            String[] array = values.split("' '");

            return array[get - 1];
        }

        return null;
    }


}
