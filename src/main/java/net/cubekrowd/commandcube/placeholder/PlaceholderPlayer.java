/*

    commandcube
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.commandcube.placeholder;

import java.util.Arrays;
import net.cubekrowd.commandcube.CommandCubePlugin;
import net.cubekrowd.commandcube.Placeholder;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class PlaceholderPlayer extends Placeholder {
    public final String playerNameNotOnline;
    public final String playerServerNotOnline;

    public PlaceholderPlayer(CommandCubePlugin plugin,
            String playerNameNotOnline, String playerServerNotOnline) {
        super(plugin, "player", "Placeholder used to get player specific information.",
                Arrays.asList("name [player]", "server [player]", "ping"));
        this.playerNameNotOnline = playerNameNotOnline;
        this.playerServerNotOnline = playerServerNotOnline;
    }

    public String getValue(CommandSender sender, String[] args) {
        if(args.length == 0) {
            return null;
        }

        if(args[0].equalsIgnoreCase("name")) {
            if(args.length == 1) {
                return sender.getName();
            } else if(args.length == 2) {
                for (var pp : ProxyServer.getInstance().getPlayers()) {
                    if (pp.getName().equalsIgnoreCase(args[1])) {
                        return pp.getName();
                    }
                }
                return playerNameNotOnline;
            }
        } else if(args[0].equalsIgnoreCase("server")) {
            if(args.length == 1) {
                if (sender instanceof ProxiedPlayer) {
                    return ((ProxiedPlayer) sender).getServer().getInfo().getName();
                }
                return null;
            } else if(args.length == 2) {
                for (var pp : ProxyServer.getInstance().getPlayers()) {
                    if (pp.getName().equalsIgnoreCase(args[1])) {
                        return pp.getServer().getInfo().getName();
                    }
                }
                return playerServerNotOnline;
            }
        } else if(args[0].equalsIgnoreCase("ping")) {
            if(sender instanceof ProxiedPlayer) {
                return "" + ((ProxiedPlayer) sender).getPing();
            } else {
                return "0";
            }
        }

        return null;
    }

}
