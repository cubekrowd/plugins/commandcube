/*

    commandcube
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.commandcube.placeholder;

import java.util.Arrays;
import java.util.stream.Collectors;
import net.cubekrowd.commandcube.CommandCubePlugin;
import net.cubekrowd.commandcube.Placeholder;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class PlaceholderServer extends Placeholder {
    public final String serverCountNotFound;
    public final String serverPlayerListNotFound;

    public PlaceholderServer(CommandCubePlugin plugin,
            String serverCountNotFound, String serverPlayerListNotFound) {
        super(plugin, "server", "Placeholder used to get server related information.",
                Arrays.asList("count [server]", "playerlist [server]"));
        this.serverCountNotFound = serverCountNotFound;
        this.serverPlayerListNotFound = serverPlayerListNotFound;
    }

    public String getValue(CommandSender sender, String[] args) {
        if (args.length == 0) {
            return null;
        }

        if (args[0].equalsIgnoreCase("count")) {
            ServerInfo si;
            if(args.length == 1) {
                si = (sender instanceof ProxiedPlayer) ?
                        ((ProxiedPlayer) sender).getServer().getInfo() : null;
            } else {
                si = ProxyServer.getInstance().getServerInfo(args[1]);
            }
            return si == null ? serverCountNotFound : si.getPlayers().size() + "";
        } else if (args[0].equalsIgnoreCase("playerlist")) {
            ServerInfo si;
            if(args.length == 1) {
                si = (sender instanceof ProxiedPlayer) ?
                        ((ProxiedPlayer) sender).getServer().getInfo() : null;
            } else {
                si = ProxyServer.getInstance().getServerInfo(args[1]);
            }

            if (si == null) {
                return serverPlayerListNotFound;
            } else {
                return si.getPlayers().stream()
                        .map(CommandSender::getName)
                        .collect(Collectors.joining(", "));
            }
        }

        return null;
    }
}
