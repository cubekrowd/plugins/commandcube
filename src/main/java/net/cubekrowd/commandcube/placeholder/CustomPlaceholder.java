/*

    commandcube
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.commandcube.placeholder;

import java.util.List;
import java.util.Map;
import net.cubekrowd.commandcube.CommandCubePlugin;
import net.cubekrowd.commandcube.Placeholder;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;

public class CustomPlaceholder extends Placeholder {

    private final CommandCubePlugin plugin;
    private final Map<String, String> statements;

    public CustomPlaceholder(CommandCubePlugin plugin, String key, String description, List<String> usage, Map<String, String> statements) {
        super(plugin, key, description, usage);
        this.plugin = plugin;
        this.statements = statements;
    }

    public String getValue(CommandSender sender, String[] args) {
        for(Map.Entry<String, String> e : statements.entrySet()) {
            if(e.getKey().equals("default")) {
                continue;
            }
            if(e.getKey().indexOf("==") == -1) {
                throw new UnsupportedOperationException("Evaluation condition does not contain evaluation statement '=='.");
            }
            String l = plugin.parsePlaceholders(sender, e.getKey().split("==")[0].trim(), args);
            String r = plugin.parsePlaceholders(sender, e.getKey().split("==")[1].trim(), args);
            if(l.equalsIgnoreCase(r)) {
                return plugin.parsePlaceholders(sender, ChatColor.translateAlternateColorCodes('&', e.getValue()), args);
            }
        }

        if(statements.keySet().contains("default")) {
            return plugin.parsePlaceholders(sender, ChatColor.translateAlternateColorCodes('&', statements.get("default")), args);
        }

        throw new UnsupportedOperationException("Custom placeholder '" + getKey() + "' does not contain 'default' condition.");
    }

}
