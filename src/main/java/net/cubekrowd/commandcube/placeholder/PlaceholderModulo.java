/*

    commandcube
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.commandcube.placeholder;

import java.util.Arrays;
import net.cubekrowd.commandcube.CommandCubePlugin;
import net.cubekrowd.commandcube.Placeholder;
import net.md_5.bungee.api.CommandSender;

public class PlaceholderModulo extends Placeholder {

    private final CommandCubePlugin plugin;

    public PlaceholderModulo(CommandCubePlugin plugin) {
        super(plugin, "modulo", "Math placeholder to calculate modulo.", Arrays.asList("<dividend> <divisor>"));
        this.plugin = plugin;
    }

    public String getValue(CommandSender sender, String[] args) {
        if(args.length == 0) {
            return null;
        }

        if(args.length == 2) {
            try {
                int dividend = Integer.parseInt(args[0]);
                int divisor = Integer.parseInt(args[1]);
                return (dividend % divisor) + "";
            } catch (Exception e) {
                return "NaN";
            }
        }

        return null;
    }

}
