/*

    commandcube
    Copyright (C) 2017  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.commandcube;

import lombok.*;
import java.util.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.plugin.*;

@AllArgsConstructor
public abstract class Placeholder {

    @NonNull
    @Getter
    private final Plugin plugin;
    @NonNull
    @Getter
    private final String key;
    @NonNull
    @Getter
    private final String description;
    @NonNull
    @Getter
    private final List<String> usage;

    public abstract String getValue(CommandSender sender, String[] args);

    public void onRegister() {}

}
