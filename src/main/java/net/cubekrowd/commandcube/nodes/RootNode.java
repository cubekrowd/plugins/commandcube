package net.cubekrowd.commandcube.nodes;

import net.md_5.bungee.api.CommandSender;


public class RootNode extends Node {

    private String[] expression;

    @Override
    public NodeType getType() {
        return NodeType.ROOT;
    }

    @Override
    public String[] getExpression() {
        return expression;
    }

    @Override
    public String execute(CommandSender sender, String[] args) {
        if (getChildren().isEmpty()) {
            return String.join(" ", expression);
        }
        StringBuilder outputstring = new StringBuilder();
        for (Node nodechild : getChildren()) {
            outputstring.append(nodechild.execute(sender, args));

        }
        return outputstring.toString();
    }

}
