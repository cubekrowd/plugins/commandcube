package net.cubekrowd.commandcube.nodes;

import net.cubekrowd.commandcube.CommandCubePlugin;
import net.cubekrowd.commandcube.Placeholder;
import net.md_5.bungee.api.CommandSender;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlaceholderNode extends Node {
    private String[] expression;
    private CommandCubePlugin plugin;


    public PlaceholderNode(String[] expression, CommandCubePlugin plugin) {
        this.expression = expression;
        this.plugin = plugin;
    }


    @Override
    public NodeType getType() {
        return NodeType.PLACEHOLDER;
    }

    @Override
    public String[] getExpression() {
        return expression;
    }

    @Override
    public String execute(CommandSender sender, String[] args) {
        // Gets rid of potential extra spaces from the NodeBuilder.
        String output = String.join(" ", NodeBuilder.removeBlankValues(expression));
        ArrayList<String> responses = new ArrayList<>();
        args = NodeBuilder.removeBlankValues(args);
        args = NodeBuilder.removeExcessSpaces(args);

        if (!getChildren().isEmpty()) {
            for (Node where : getChildren()) {
                responses.add(where.execute(sender, args));
            }
            // Filters through the responses to remove extra spacing/not enough args.

            responses = removeDoubleSpaces(responses);
            String[] results = responses.toArray(new String[0]);

            String temp = String.join(" ", results);
            results = temp.split(" ");
            results = NodeBuilder.removeExcessSpaces(results);


            output = parseFunctions(sender, String.join(" ", results), args);

        } else {
            String temp = String.join(" ", args);
            String[] results = temp.split(" ");
            results = NodeBuilder.removeExcessSpaces(results);

            output = parseFunctions(sender, output, results);
        }
        output = output.replace("  ", " ");
        return output;
    }

    private String parseFunctions(CommandSender sender, String input, String[] args) {
        // ? returns all the args.
        if (input.equalsIgnoreCase("?")) {
            return String.join(" ", args);
        }
        for (int i = 0; i < args.length; i++) {
            // # returns a singular argument.
            if (input.equalsIgnoreCase((i + 1) + "")) {
                return args[i];
            }
        }

        synchronized (plugin.globalLock) {
            String value = null;
            String[] inputarray = input.split(" ");
            // Parses through all the placeholders.
            for (Placeholder ph : plugin.placeholders) {
                if (ph.getKey().equalsIgnoreCase(inputarray[0])) {
                    value = ph.getValue(sender, Arrays.copyOfRange(inputarray, 1, inputarray.length));
                    if (value != null) {
                        return value;
                    }
                }
            }
            return input;
        }
    }


    private ArrayList<String> removeDoubleSpaces(List<String> input) {
        ArrayList<String> filtered = new ArrayList<>();
        for (String check : input) {
            String filteredstring = check.replace("  ", " ");
            filtered.add(filteredstring);
        }
        return filtered;
    }

}
