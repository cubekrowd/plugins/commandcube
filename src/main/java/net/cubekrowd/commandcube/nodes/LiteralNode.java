package net.cubekrowd.commandcube.nodes;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;

public class LiteralNode extends Node {
    private String[] expressionarray;
    private String expression;
    private NodeType ownerType;

    public LiteralNode(String data, NodeType ownerType) {
        data = ChatColor.translateAlternateColorCodes('&', data);
        expressionarray = data.split(" ");
        expression = data;
        this.ownerType = ownerType;
    }

    @Override
    public NodeType getType() {
        return NodeType.LITERAL;
    }

    @Override
    public String[] getExpression() {
        return expressionarray;
    }

    @Override
    public String execute(CommandSender sender, String[] args) {
        // Checks to see if it comes from a function because functions can't have extra spaces or anything that the String itself could just return.
        if (ownerType == NodeType.PLACEHOLDER) {
            expressionarray = NodeBuilder.removeExcessSpaces(expressionarray);
            return String.join(" ", expressionarray);
        }
        return expression;
    }


}
