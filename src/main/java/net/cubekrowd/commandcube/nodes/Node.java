package net.cubekrowd.commandcube.nodes;

import net.md_5.bungee.api.CommandSender;

import java.util.ArrayList;
import java.util.List;

public abstract class Node {
    public ArrayList<Node> child = new ArrayList<>();

    public abstract NodeType getType();

    public abstract String execute(CommandSender sender, String[] args);

    public abstract String[] getExpression();

    public void addChild(Node node) {
        child.add(node);
    }

    public List<Node> getChildren() {
        return child;
    }


}

