package net.cubekrowd.commandcube.nodes;

import net.cubekrowd.commandcube.CommandCubePlugin;

import java.util.ArrayList;


public class NodeBuilder {

    private RootNode root;
    private CommandCubePlugin plugin;
    private String input;

    public NodeBuilder(String input, CommandCubePlugin plugin) {
        this.plugin = plugin;
        root = new RootNode();
        this.input = input;

    }


    private void parseForFunctions(String input, Node node) {
        // These 3 variables are to keep track of the functions and the literals.
        ArrayList<Integer> open = new ArrayList<>();
        ArrayList<Integer> closed = new ArrayList<>();
        StringBuilder literal = new StringBuilder();

        boolean doagain = false;
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            if (ch == '$' && input.charAt(i + 1) == '{') {
                // Literals will only ever happen on the root node, so it builds the literal when the node branches.
                if (open.isEmpty() && node.getType() != NodeType.LITERAL) {
                    if (literal.length() > 0) {
                        node.addChild(new LiteralNode(literal.toString(), node.getType()));
                        literal.setLength(0);
                    }
                }

                // Keeps track of where the open brackets are.
                open.add(i);
                i++;

            } else {
                if (ch == '}') {
                    // It can't be a function without an opening bracket.
                    if (!open.isEmpty()) {
                        closed.add(i);
                        if (open.size() == closed.size()) {
                            String bubble = input.substring(open.get(0) + 2, i);
                            String[] args = bubble.split("-=-");

                            node.addChild(new PlaceholderNode(args, plugin));


                            open.clear();
                            closed.clear();
                        } else {
                            // If there is any child functions inside of the function.
                            doagain = true;
                        }

                    }

                } else {
                    if (open.isEmpty() && node.getType() != NodeType.LITERAL) {
                        literal.append(ch);
                    }
                }
            }
        }
        if (open.isEmpty() && node.getType() != NodeType.LITERAL) {
            // At end there can be text, just double checks literals.
            if (!literal.toString().equals("") && !literal.toString().isEmpty()) {
                node.addChild(new LiteralNode(literal.toString(), node.getType()));
            }
        }
        if (doagain) {
            // Makes the function recursive.
            for (Node child : node.getChildren()) {
                if (child.getType() != NodeType.LITERAL) {
                    parseForFunctions(String.join(" ", child.getExpression()), child);
                }
            }
        }
    }


    public RootNode build() {
        // After everything is done, you get a RootNode object that contains all of the data.
        parseForFunctions(input, root);
        return root;
    }

    public static String[] removeBlankValues(String[] args) {
        ArrayList<String> filtered = new ArrayList<>();
        for (String arg : args) {
            if (arg != null && !arg.isEmpty() && !arg.equals(" ")) {
                filtered.add(arg);
            }
        }
        return filtered.toArray(new String[0]);

    }

    public static String[] removeExcessSpaces(String[] args) {
        ArrayList<String> filtered = new ArrayList<>();
        args = removeBlankValues(args);
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            boolean filter = false;
            if (arg.charAt(0) == ' ') {
                filtered.add(arg.substring(1, arg.length() - 1));
                filter = true;
            }
            if (arg.charAt(arg.length() - 1) == ' ' && !filter) {
                filtered.add(arg.substring(0, arg.length() - 2));
                filter = true;
            }
            if (!filter) {
                filtered.add(arg);
            }
        }
        return filtered.toArray(new String[0]);
    }


}
